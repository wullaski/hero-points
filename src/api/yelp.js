import axios from 'axios';

export default axios.create({
    baseURL: 'https://api.yelp.com/v3/businesses',
    headers: {
        Authorization: 'Bearer p1wkeb4XqmxgwWZ6zx-j0m3Kgdiu28LPNdCJYh08UuTzGIu20F72SeLR8JIfmGlihlbgHfIpRUcgbcrUI67YN-byuD8fgmfQw3HjxKwq8BZ6qKdjV2SYkRKmvzh7YXYx'
    }
});
