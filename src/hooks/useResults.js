import { useEffect, useState } from 'react';
import yelp from '../api/yelp';

export default () => {
    const [results, setResults] = useState([]);
    const [errorMessage, setErrorMessage] = useState('');

    const searchApi = async (searchTerm) => {

        try {
            const response = await yelp.get('/search', {
                params: {
                    limit: 50,
                    term: searchTerm,
                    location: 'syracuse'
                }
            });
            setResults(response.data.businesses);
            setErrorMessage(null);
        } catch (err) {
            setErrorMessage('Oh noes, app broke');
        }
    };

    useEffect(() => {
        searchApi('Dining');
    }, []);

    return [searchApi, results, errorMessage]
}